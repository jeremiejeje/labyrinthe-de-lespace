<!doctype html>
<html>
<head>
<?php
session_start();


/**
 * partie enregistrement dans la base de données
 */
$pdo = new PDO('mysql:dbname=labyrinthe;host=127.0.0.1', 'root', '');
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$user_id = $_SESSION['auth']['id'];



/**
 * partie deco
 */
if(!empty($_GET['logout'])){
    unset($_SESSION['auth']);
    header('Location: ../');die();
}

?>

<meta charset="utf-8">
<title>Le labyrinthe de l'espace</title>
<link rel="stylesheet" href="css/bootstrap.css" />
<link rel="stylesheet" href="css/style.css" />


</head>
<body id='body'>
<form action="#" method="get">
    <input type="submit" name="logout" value="Se deconnecter">
</form>

<?php 




 ?>
<div class="niv1">niveau 1</div>
<div class="scorefinal">
	<table class="table table-striped">
	  <thead>
	    <tr>
	      <th>Nom / Prénom</th>
	      <th>Nb pièce : </th>
	      <th>Nb déplacement : </th>
	      <th>Win or Fail </th>
	    </tr>
	  </thead>
	  <tbody>
	  	
	      
	      <?php


		$statement = "SELECT * FROM users";
		$query = $pdo->query($statement);
	

		
		?>

		
			<?php	

			while($result = $query->fetch()){	 ?>
			<tr>
				<td><?php echo $result['nom']."\n".$result['prenom']; ?></td>
				<?php
				$id = $result['id']; 
				$statement = "SELECT * FROM score WHERE id='$id'";
				$query2 = $pdo->query($statement);
				$result2 = $query2->fetch();
				?>
				<td><?php echo $result2['piece']; ?></td>
				<td><?php echo $result2['deplacement']; ?></td>
				<td><?php if($result2['piece'] == 0 && $result2['piece'] != null){
					echo "Win";
				}else{
					echo "fail";
				} ?></td>
			</tr>
				<?php


			}
				
		      ?>	
	
	
	      
	    <tr>
	      <td>Larry</td>
	      <td>the Bird</td>
	      <td>@twitter</td>
	    </tr>
	    
	  </tbody>
	</table>

</div>


<div class="niv1">niveau 2</div>
<div class="scorefinal">
	<table class="table table-striped">
	  <thead>
	    <tr>
	      <th>Nom / Prénom</th>
	      <th>Nb pièce : </th>
	      <th>Nb déplacement : </th>
	      <th>Win or Fail </th>
	    </tr>
	  </thead>
	  <tbody>
	  	
	      
	      <?php


		$statement = "SELECT * FROM users";
		$query = $pdo->query($statement);
	

		
		?>

		
			<?php	

			while($result = $query->fetch()){	 ?>
			<tr>
				<td><?php echo $result['nom']."\n".$result['prenom']; ?></td>
				<?php
				$id = $result['id']; 
				$statement = "SELECT * FROM score WHERE id='$id'";
				$query2 = $pdo->query($statement);
				$result2 = $query2->fetch();
				?>
				<td><?php echo $result2['piece2']; ?></td>
				<td><?php echo $result2['deplacement2']; ?></td>
				<td><?php if($result2['piece2'] == 0 && $result2['piece2'] != null){
					echo "Win";
				}else{
					echo "fail";
				} ?></td>
			</tr>
				<?php


			}
				
		      ?>	
	
	
	      
	    <tr>
	      <td>Larry</td>
	      <td>the Bird</td>
	      <td>@twitter</td>
	    </tr>
	    
	  </tbody>
	</table>

</div>

<div class="niv1">niveau 3</div>
<div class="scorefinal">
	<table class="table table-striped">
	  <thead>
	    <tr>
	      <th>Nom / Prénom</th>
	      <th>Nb pièce : </th>
	      <th>Nb déplacement : </th>
	      <th>Win or Fail </th>
	    </tr>
	  </thead>
	  <tbody>
	  	
	      
	      <?php


		$statement = "SELECT * FROM users";
		$query = $pdo->query($statement);
	

		
		?>

		
			<?php	

			while($result = $query->fetch()){	 ?>
			<tr>
				<td><?php echo $result['nom']."\n".$result['prenom']; ?></td>
				<?php
				$id = $result['id']; 
				$statement = "SELECT * FROM score WHERE id='$id'";
				$query2 = $pdo->query($statement);
				$result2 = $query2->fetch();
				?>
				<td><?php echo $result2['piece3']; ?></td>
				<td><?php echo $result2['deplacement3']; ?></td>
				<td><?php if($result2['piece3'] == 0 && $result2['piece3'] != null){
					echo "Win";
				}else{
					echo "fail";
				} ?></td>
			</tr>
				<?php


			}
				
		      ?>	
	
	
	      
	    <tr>
	      <td>Larry</td>
	      <td>the Bird</td>
	      <td>@twitter</td>
	    </tr>
	    
	  </tbody>
	</table>

</div>





</body>
</html>
