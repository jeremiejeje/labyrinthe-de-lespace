<!doctype html>
<html>
<head>
<?php
session_start();

/**
 * verif de l'auth
 */
if(isset($_SESSION['auth'])){

}else{
	header('Location: ../');
}
/**
 * partie enregistrement dans la base de données
 */
$pdo = new PDO('mysql:dbname=labyrinthe;host=127.0.0.1', 'root', '');
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$user_id = $_SESSION['auth']['id'];

$statement = "SELECT * FROM score WHERE id = '$user_id'";
$query = $pdo->query($statement);

if(($query->rowCount()) == 0){
    $statement = "INSERT INTO score (id) VALUES ('$user_id')";
    $pdo->query($statement);
}



/**
 * partie deco
 */
if(!empty($_GET['logout'])){
	unset($_SESSION['auth']);
	header('Location: ../');die();
}
 ?>

<meta charset="utf-8">

<title>Le labyrinthe de l'espace</title>
<script src="js/ajax.js"></script>
<script src="js/app.js"></script>
<script src="js/personnage.js"></script>
<link rel="stylesheet" href="css/style.css" />



</script>


</head>
<body id='body'>
	<form action="#" method="get">
		<input type="submit" name="logout" value="Se deconnecter">
	</form>
<div class="base-interraction" >
	<h2 style="color:white">Le labyrinthe de l’espace niv 1</h2>
    <p style="color:#BDC3C7;"> Information : </p><div style="inline-block;" class="interraction"></div>

</div>




<div class="persos"></div>

<div class="tab-controle left">
	<p style="text-align:center"><strong>Explication du fonctionnement de bonus :</strong></p>
    <p class="align">Lorsque vous ramassez une case mystère vous pouvez alors gagner de manière aléatoire un bonus parmi les 6 suivants : <img id='img' src="img/mystere.jpg"/></p>
    <div class="tab-pimg">
        <p>Bonus qui vous permet de casser un unique mur en cliquant sur le mur de votre choix, attention vous ne pouvez casser uniquement les murs à l'intérieur de la map, vous ne pourrez pas vous échapper de la map!! <img src="img/CasserUnMur.JPG"/></p>
        <p>Ce bonus vous fait gagner 5 déplacements supplémentaires<img src="img/PlusDeDeplacement.JPG"/></p>
        <p>Avec ce bonus vous avez la possibilité de vous téléporter sur une case de la map de votre choix en cliquant dessus, uniquement à condition que ce ne soit pas un mur <img src="img/Teleportation.JPG"/></p>
        <p>Bonus permettant de gagner 3 pièces supplémentaires <img src="img/PlusDePiece.JPG"/></p>
        <p>Une vision du personnage aggrandit à 4 cases autour de lui, attention ce bonus ne dure qu'un unique tour <img src="img/VisionAgrandit.JPG"/></p>
        <p>Ce bonus enlevera trois pièges sur la map, vous ne vous en rendrez certainement pas compte car les pièges sont invisibles même avec la vision mais ce bonus peut s'avérer très utile <img src="img/chemin.jpg"/></p>
	</div>
</div>

<div class="tab-controle">
	<div class="piece"><img src="img/piece.png" width='50'/><div class="tab-texte" >Nombre de pièce à trouver :<span class="nb-piece">10 </span></div></div>
	<div><img src="img/deplacement.png" />Nombre de déplacement restant : <span class="deplacement">50</span></div>
    <h3 style="text-align:center">Bonus</h3>
    <div class="tab-bonus tab-cassemur"><img src="img/CasserUnMur.JPG" />Vous avez actuellement la possibilité de casser un mur en cliquant dessus</div>
    <div class="tab-bonus tab-plusdep"><img src="img/PlusDeDeplacement.JPG" />Vous venez de gagner 5 déplacement supplémentaires</div>
    <div class="tab-bonus tab-teleportation"><img src="img/Teleportation.JPG" />Vous avez actuellement la possibilité de vous téléporter sur une case en cliquant dessus</div>
    <div class="tab-bonus tab-pluspiece"><img src="img/PlusDePiece.JPG" />Vous venez de gagner 3 pièces supplémentaires</div>
    <div class="tab-bonus tab-visionplus"><img src="img/VisionAgrandit.JPG" />Vous disposez actuellement d'une vision aggrandit ( uniquement pour ce tour ! )</div>
    <div class="tab-bonus tab-moinspiege"><img src="img/chemin.jpg" />3 pièges viennent de disparaitre de la map, vous avez de la chance !!</div>
     <div class="tab-bonus tab-piege"><img src="img/piegeleslie.jpg" /><h1 style='color:red;'>Vous avez marché sur un piège vous perdez une pièce !!</h1></div>
</div>



</body>
</html>
